package com.bkboiz.shopappbackend.repository;

import com.bkboiz.shopappbackend.entity.VnPayTracking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VnPayTrackingRepository extends JpaRepository<VnPayTracking, Long> {
}
