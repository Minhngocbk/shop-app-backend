package com.bkboiz.shopappbackend.repository;

import com.bkboiz.shopappbackend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByName(String productName);

    @Query("select p from Product p " +
            "where (:keyword is null or :keyword = '' or p.name like %:keyword% or p.description like %:keyword%) " +
            "and (:categoryId is null or :categoryId = 0 or p.categoryId = :categoryId)")
    Page<Product> search(String keyword, Long categoryId, Pageable pageRequest);
}
