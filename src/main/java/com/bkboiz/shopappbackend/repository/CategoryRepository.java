package com.bkboiz.shopappbackend.repository;

import com.bkboiz.shopappbackend.entity.Category;
import com.bkboiz.shopappbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("select c.name from Category c where c.id = ?1")
    String getCategoryName(Long id);
}
