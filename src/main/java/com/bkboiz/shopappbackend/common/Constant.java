package com.bkboiz.shopappbackend.common;

public class Constant {
    public static String AUTHORIZATION_HEADER = "Authorization";
    public static String TOKEN_PREFIX = "Bearer ";
}
