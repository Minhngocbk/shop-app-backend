package com.bkboiz.shopappbackend.common;

public enum OrderStatus {
    PENDING,
    SHIPPING,
    DELIVERING,
    DELIVERED
}
