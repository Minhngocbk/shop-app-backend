package com.bkboiz.shopappbackend.controller;

import com.bkboiz.shopappbackend.dto.ProductDTO;
import com.bkboiz.shopappbackend.entity.Product;
import com.bkboiz.shopappbackend.entity.ProductImage;
import com.bkboiz.shopappbackend.service.IProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/product")
@RequiredArgsConstructor
public class ProductController {

    private final IProductService productService;

    @GetMapping
    public ResponseEntity<Object> getProducts(@RequestParam(defaultValue = "0") int page,
                                              @RequestParam(defaultValue = "10") int limit,
                                              @RequestParam(defaultValue = "") String keyword,
                                              @RequestParam(defaultValue = "0") Long categoryId) {
        return ResponseEntity.ok(productService.getProducts(page, limit, keyword, categoryId));
    }

    @PostMapping
    public ResponseEntity<Object> createProduct(@Valid @RequestBody ProductDTO product,
                                        BindingResult result) {
        if (result.hasErrors()) {
            List<String> errMsgs = result.getFieldErrors()
                    .stream()
                    .map(FieldError::getDefaultMessage)
                    .toList();
            return ResponseEntity.badRequest().body(errMsgs);
        }
        return ResponseEntity.ok(productService.create(product));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Long id) {
        return ResponseEntity.ok(productService.getProductById(id));
    }

    @PostMapping(value = "uploads/{id}",
                consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadImage(@PathVariable("id") Long productId,
                                              @ModelAttribute("files") List<MultipartFile> files) {
        return ResponseEntity.ok(productService.uploadImage(productId, files));
    }

    @GetMapping("images/{imageName}")
    public ResponseEntity<Object> viewImage(@PathVariable String imageName) {
        try {
            Path imagePath = Paths.get("uploads/" + imageName);
            UrlResource resource = new UrlResource(imagePath.toUri());

            if (resource.exists()) {
                return ResponseEntity.ok()
                        .contentType(MediaType.IMAGE_JPEG)
                        .body(resource);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("fake")
    public ResponseEntity<Object> fake() {
        return ResponseEntity.ok(productService.fake());
    }
}
