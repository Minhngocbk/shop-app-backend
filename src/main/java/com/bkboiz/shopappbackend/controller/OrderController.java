package com.bkboiz.shopappbackend.controller;

import com.bkboiz.shopappbackend.dto.OrderDTO;
import com.bkboiz.shopappbackend.dto.request.OrderRequest;
import com.bkboiz.shopappbackend.service.IOrderService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/order")
@RequiredArgsConstructor
public class OrderController {

    private final IOrderService orderService;

    @PostMapping
    public ResponseEntity<Object> order(@Valid @RequestBody OrderRequest orderRequest,
                                BindingResult result) {
        if (result.hasErrors()) {
            List<String> errMsgs = result.getFieldErrors()
                    .stream()
                    .map(FieldError::getDefaultMessage)
                    .toList();
            return ResponseEntity.badRequest().body(errMsgs);
        }

        return ResponseEntity.ok(orderService.order(orderRequest));
    }

    @GetMapping
    public ResponseEntity<Object> getOrderByUserId(@RequestParam Long userId) {
        return ResponseEntity.ok(orderService.findOrderByUserId(userId));
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Object> getOrder(@PathVariable Long orderId) {
        return ResponseEntity.ok(orderService.getOrder(orderId));
    }

    @PutMapping("update")
    public ResponseEntity<Object> updateOrder(@RequestParam Long id,
                                              @RequestBody OrderDTO orderRequest) {
        return ResponseEntity.ok(orderService.updateOrder(id, orderRequest));
    }
}
