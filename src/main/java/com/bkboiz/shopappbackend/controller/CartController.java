package com.bkboiz.shopappbackend.controller;

import com.bkboiz.shopappbackend.dto.CartDTO;
import com.bkboiz.shopappbackend.dto.CategoryDTO;
import com.bkboiz.shopappbackend.service.ICartService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/cart")
@RequiredArgsConstructor
public class CartController {

    private final ICartService cartService;

    @PostMapping
    public ResponseEntity addToCart(@Valid @RequestBody CartDTO cartDTO,
                                         BindingResult result) {
        if (result.hasErrors()) {
            List<String> errMsgs = result.getFieldErrors()
                    .stream()
                    .map(FieldError::getDefaultMessage)
                    .toList();
            return ResponseEntity.badRequest().body(errMsgs);
        }
        return ResponseEntity.ok(cartService.addToCart(cartDTO));
    }

    @GetMapping
    public ResponseEntity getByUserId(@RequestParam Long userId) {
        return ResponseEntity.ok(cartService.findByUserId(userId));
    }
    
}
