package com.bkboiz.shopappbackend.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api.prefix}/vnpay")
public class VnPayController {

    @GetMapping("return")
    public ResponseEntity vnPayReturn() {
        return null;
    }
}
