package com.bkboiz.shopappbackend.service;

import com.bkboiz.shopappbackend.dto.ProductDTO;
import com.bkboiz.shopappbackend.entity.Product;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface IProductService {
    Product create(ProductDTO product);

    Object getProductById(Long id);

    Object uploadImage(Long productId, List<MultipartFile> files);

    Object getProducts(int page, int limit, String keyword, Long categoryId);

    Object fake();
}
