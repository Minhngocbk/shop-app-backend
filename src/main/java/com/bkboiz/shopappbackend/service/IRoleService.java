package com.bkboiz.shopappbackend.service;

import com.bkboiz.shopappbackend.entity.Role;

import java.util.List;

public interface IRoleService {
    List<Role> findAll();
}
