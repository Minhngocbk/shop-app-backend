package com.bkboiz.shopappbackend.service;

import com.bkboiz.shopappbackend.entity.VnPayTracking;
import org.springframework.stereotype.Service;

@Service
public interface IVnPayTrackingService {
    Object save(VnPayTracking vnPayTracking);
}
