package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.dto.OrderDetailDTO;
import com.bkboiz.shopappbackend.entity.OrderDetail;
import com.bkboiz.shopappbackend.repository.OrderDetailRepository;
import com.bkboiz.shopappbackend.service.IOrderDetailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j
@Service
@RequiredArgsConstructor
public class OrderDetailService implements IOrderDetailService {

    private final OrderDetailRepository detailRepo;
    private final ModelMapper modelMapper;
    @Override
    public List<OrderDetailDTO> findByOrderId(Long orderId) {
        List<OrderDetail> orderDetails = detailRepo.findByOrderId(orderId);
        return orderDetails.stream()
                .map(orderDetail -> modelMapper.map(orderDetail, OrderDetailDTO.class))
                .toList();
    }
}
