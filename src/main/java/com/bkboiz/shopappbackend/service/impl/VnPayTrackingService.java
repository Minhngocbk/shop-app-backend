package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.entity.VnPayTracking;
import com.bkboiz.shopappbackend.repository.VnPayTrackingRepository;
import com.bkboiz.shopappbackend.service.IVnPayTrackingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VnPayTrackingService implements IVnPayTrackingService {
    private final VnPayTrackingRepository vnPayTrackingRepo;

    @Override
    public Object save(VnPayTracking vnPayTracking) {
        return vnPayTrackingRepo.save(vnPayTracking);
    }
}
