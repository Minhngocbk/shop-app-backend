package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.dto.CartDTO;
import com.bkboiz.shopappbackend.dto.response.CartResponse;
import com.bkboiz.shopappbackend.entity.Cart;
import com.bkboiz.shopappbackend.repository.CartRepository;
import com.bkboiz.shopappbackend.service.ICartService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j
@Service
@RequiredArgsConstructor
public class CartService implements ICartService {
    private final CartRepository cartRepository;
    private final ModelMapper mapper;

    @Override
    public Object findByUserId(Long userId) {
        List<Cart> lstCart = cartRepository.findByUserId(userId);

        return lstCart.stream()
                .map(cart -> mapper.map(cart, CartResponse.class))
                .toList();
    }

    @Override
    public Object addToCart(@Valid CartDTO cartDTO) {
        Optional<Cart> existCart = cartRepository.findByUserIdAndProductId(cartDTO.getUserId(), cartDTO.getProductId());
        Cart cart;
        if (existCart.isPresent()) {
            cart = existCart.get();
            cart.setQuantity(cart.getQuantity() + cartDTO.getQuantity());
        } else {
            cart = new Cart();
            cart.setUserId(cartDTO.getUserId());
            cart.setProductId(cartDTO.getProductId());
            cart.setQuantity(cartDTO.getQuantity());
        }

        return cartRepository.saveAndFlush(cart);
    }
}
