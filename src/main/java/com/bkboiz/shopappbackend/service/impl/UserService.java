package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.dto.UserDTO;
import com.bkboiz.shopappbackend.dto.UserLoginDTO;
import com.bkboiz.shopappbackend.dto.UserResponse;
import com.bkboiz.shopappbackend.dto.response.UserLoginResponse;
import com.bkboiz.shopappbackend.entity.User;
import com.bkboiz.shopappbackend.repository.UserRepository;
import com.bkboiz.shopappbackend.security.jwt.JwtService;
import com.bkboiz.shopappbackend.service.IUserService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.apache.coyote.BadRequestException;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Log4j
@Service
@RequiredArgsConstructor
public class UserService implements IUserService {
    private final ModelMapper mapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authManager;
    private final UserRepository userRepo;
    private final Gson gson;

    @Override
    public Object register(UserDTO userDTO) {
        log.info("<<register>>: request = " + gson.toJson(userDTO));
        if (userRepo.findByPhoneNumber(userDTO.getPhoneNumber()).isPresent()) {
            return ResponseEntity.badRequest().body("So dien thoai da ton tai");
        }
        User user = mapper.map(userDTO, User.class);
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRoleId(1);
        user.setCreatedAt(new Date(System.currentTimeMillis()));
        user.setUpdatedAt(new Date(System.currentTimeMillis()));

        User savedUser = userRepo.save(user);
        log.info("<<register>>: response = " + gson.toJson(savedUser));

        return savedUser;
    }

    @SneakyThrows
    @Override
    public Object login(UserLoginDTO userLoginDTO) {
        log.info("<<login>>: request = " + gson.toJson(userLoginDTO));
        Optional<User> opUser = userRepo.findByPhoneNumber(userLoginDTO.getPhoneNumber());
        if (opUser.isEmpty()) {
            throw new UsernameNotFoundException("Wrong phone/password");
        }

        if (!passwordEncoder.matches(userLoginDTO.getPassword(), opUser.get().getPassword())) {
            throw new BadRequestException("Wrong phone/password");
        }

        authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userLoginDTO.getPhoneNumber(), userLoginDTO.getPassword()));

        User loginUser = opUser.get();

        UserLoginResponse loginResponse = UserLoginResponse.builder()
                .userId(loginUser.getId())
                .fullName(loginUser.getFullName())
                .phoneNumber(loginUser.getPhoneNumber())
                .accessToken(jwtService.generateToken(userLoginDTO.getPhoneNumber()))
                .build();
        log.info("<<login>>: response = " + gson.toJson(loginResponse));

        return loginResponse;
    }
}
