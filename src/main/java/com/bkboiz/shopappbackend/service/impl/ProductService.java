package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.dto.ProductDTO;
import com.bkboiz.shopappbackend.dto.response.ListProductResponse;
import com.bkboiz.shopappbackend.dto.response.ProductResponse;
import com.bkboiz.shopappbackend.entity.Category;
import com.bkboiz.shopappbackend.entity.Product;
import com.bkboiz.shopappbackend.entity.ProductImage;
import com.bkboiz.shopappbackend.exception.DataNotFoundException;
import com.bkboiz.shopappbackend.repository.CategoryRepository;
import com.bkboiz.shopappbackend.repository.ProductImageRepository;
import com.bkboiz.shopappbackend.repository.ProductRepository;
import com.bkboiz.shopappbackend.service.IProductService;
import com.bkboiz.shopappbackend.utils.FileUtils;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Log4j
@Service
@RequiredArgsConstructor
public class ProductService implements IProductService {

    private final ProductRepository productRepo;
    private final ProductImageRepository productImageRepo;
    private final CategoryRepository categoryRepo;
    private final FileUtils fileUtils;

    @Override
    public Product create(ProductDTO product) {
        Category category = categoryRepo.findById(product.getCategoryId())
                .orElseThrow(() -> new DataNotFoundException("Can not find category with id = " + product.getCategoryId()));
        Product product1 = Product.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .thumbnail(product.getThumbnail())
                .categoryId(category.getId())
                .build();
        return productRepo.save(product1);
    }

    @Override
    public ProductResponse getProductById(Long id) {
        return mapToProductResponse(productRepo.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Product not found")));
    }

    @Override
    public Object uploadImage(Long productId, List<MultipartFile> files) {
        try {
            Product existingProduct = productRepo.findById(productId)
                    .orElseThrow(() -> new DataNotFoundException("Product not found"));
            files = files == null ? new ArrayList<>() : files;
            if (files.size() > ProductImage.MAXIMUM_IMAGES_PER_PRODUCT) {
                return ResponseEntity.badRequest().body("Exceed maximum images for product");
            }
            List<ProductImage> productImages = new ArrayList<>();
            for (MultipartFile file : files) {
                if (file.getSize() == 0) {
                    continue;
                }
                if (file.getSize() > 10 * 1024 * 1024) { // > 10MB
                    return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE).body("Size must be less than 10 MB");
                }
                String contentType = file.getContentType();
                if (contentType == null || !contentType.startsWith("image/")) {
                    return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build();
                }
                String fileName = fileUtils.saveFile(file);
                ProductImage productImage = ProductImage.builder()
                        .productId(existingProduct.getId())
                        .imageUrl(fileName)
                        .build();

                productImages.add(productImage);
            }
            return productImages;
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Object getProducts(int page, int limit, String keyword, Long categoryId) {
        PageRequest pageRequest = PageRequest.of(page - 1,
                limit,
                Sort.by("id").ascending()
        );
        Page<Product> pages = productRepo.search(keyword, categoryId, pageRequest);
//        Page<Product> pages = productRepo.findAll(pageRequest);
        List<ProductResponse> productResponses = pages.getContent().stream()
                .map(this::mapToProductResponse)
                .toList();
        return ListProductResponse.builder()
                .totalPages(pages.getTotalPages())
                .lstProduct(productResponses)
                .build();
    }

    @Override
    public Object fake() {
        Faker faker = Faker.instance();
        List<Product> productList = new ArrayList<>();
        Set<String> productExisted = new HashSet<>();
        for (int i = 0; i < 1000; i++) {
            String productName = faker.commerce().productName();
            if (productExisted.contains(productName)) {
                continue;
            }
            productExisted.add(productName);
            Product product = Product.builder()
                    .name(productName)
                    .description(faker.lorem().sentence())
                    .price((float) faker.number().numberBetween(1, 10000))
                    .thumbnail("")
                    .categoryId((long) faker.number().numberBetween(1, 5))
                    .build();
            productList.add(product);
        }
        List<Product> savedList = productRepo.saveAll(productList);

        return savedList.get(0);
    }

    private ProductResponse mapToProductResponse(Product product) {
        List<ProductImage> imagesProduct = productImageRepo.findByProductId(product.getId());
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .thumbnail(product.getThumbnail())
                .price(product.getPrice())
                .categoryName(categoryRepo.getCategoryName(product.getCategoryId()))
                .productImages(imagesProduct.stream()
                        .map(ProductImage::getImageUrl).toList())
                .build();
    }

    private boolean isExist(String productName) {
        Optional<Product> byName = productRepo.findByName(productName);
        return byName.isPresent();
    }
}
