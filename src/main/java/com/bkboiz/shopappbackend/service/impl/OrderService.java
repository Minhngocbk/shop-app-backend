package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.common.OrderStatus;
import com.bkboiz.shopappbackend.dto.OrderDTO;
import com.bkboiz.shopappbackend.dto.request.OrderDetailRequest;
import com.bkboiz.shopappbackend.dto.request.OrderRequest;
import com.bkboiz.shopappbackend.dto.response.OrderResponse;
import com.bkboiz.shopappbackend.entity.Order;
import com.bkboiz.shopappbackend.entity.OrderDetail;
import com.bkboiz.shopappbackend.entity.User;
import com.bkboiz.shopappbackend.exception.DataNotFoundException;
import com.bkboiz.shopappbackend.repository.OrderDetailRepository;
import com.bkboiz.shopappbackend.repository.OrderRepository;
import com.bkboiz.shopappbackend.repository.UserRepository;
import com.bkboiz.shopappbackend.service.IOrderService;
import com.bkboiz.shopappbackend.utils.VNPay;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log4j
@Service
@RequiredArgsConstructor
public class OrderService implements IOrderService {
    private final OrderRepository orderRepo;
    private final OrderDetailRepository orderDetailRepo;
    private final UserRepository userRepo;
    private final ModelMapper modelMapper;
    private final VNPay vnPay;
    private final Gson gson;

    @Override
    @Transactional
    public Object order(OrderRequest orderRequest) {
        log.info("<<order>>: request = " + gson.toJson(orderRequest));

        Order order = Order.builder()
                .userId(orderRequest.getUserId())
                .fullName(orderRequest.getFullName())
                .email(orderRequest.getEmail())
                .phoneNumber(orderRequest.getPhoneNumber())
                .address(orderRequest.getAddress())
                .shippingAddress(orderRequest.getShippingAddress())
                .orderDate(new Date())
                .note(orderRequest.getNote())
                .shippingMethod(orderRequest.getShippingMethod())
                .paymentMethod(orderRequest.getPaymentMethod())
                .totalMoney(BigDecimal.valueOf(orderRequest.getTotalMoney()))
                .status(OrderStatus.PENDING.name())
                .build();

        Order savedOrder = orderRepo.save(order);

        List<OrderDetailRequest> lstOrderDetailRequest = orderRequest.getLstOrderDetail();

        List<OrderDetail> orderDetails = lstOrderDetailRequest.stream()
                .map(orderDetailRequest -> {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.setOrderId(savedOrder.getId());
                    orderDetail.setProductId(orderDetailRequest.getProductId());
                    orderDetail.setQuantity(orderDetailRequest.getQuantity());
                    orderDetail.setPrice(BigDecimal.valueOf(orderDetailRequest.getPrice()));
                    orderDetail.setTotalMoney(BigDecimal.valueOf(orderDetailRequest.getTotalMoney()));
                    return orderDetail;
                }).toList();

        orderDetailRepo.saveAll(orderDetails);

        String url = vnPay.putParam(orderRequest, savedOrder.getId());
        log.info("<<order>>: url_payment = " + url);
        return url;
    }

    @Override
    public Object createOrder(OrderDTO orderDTO) {
        User user = userRepo.findById(orderDTO.getUserId())
                .orElseThrow(() -> new DataNotFoundException("User id not found"));

        Order order = modelMapper.map(orderDTO, Order.class);

        order.setUserId(user.getId());
        order.setOrderDate(new Date());
        order.setStatus(OrderStatus.PENDING.name());
        order.setIsActive(true);

        orderRepo.save(order);

        return modelMapper.map(order, OrderResponse.class);
    }

    @Override
    public Object updateOrder(Long id, OrderDTO orderDTO) {
        Order existingOrder = orderRepo.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Order not found"));
        return null;
    }

    @Override
    public OrderDTO getOrder(Long orderId) {
        Order order = orderRepo.findById(orderId)
                .orElseThrow(() -> new DataNotFoundException("Order not found"));
        return modelMapper.map(order, OrderDTO.class);
    }

    @Override
    public List<OrderDTO> findOrderByUserId(Long userId) {
        List<Order> orders = orderRepo.findByUserId(userId);
        return orders.stream()
                .map(order -> modelMapper.map(order, OrderDTO.class))
                .toList();
    }

}
