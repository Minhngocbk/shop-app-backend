package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.dto.CategoryDTO;
import com.bkboiz.shopappbackend.dto.response.CategoryResponse;
import com.bkboiz.shopappbackend.entity.Category;
import com.bkboiz.shopappbackend.exception.DataNotFoundException;
import com.bkboiz.shopappbackend.repository.CategoryRepository;
import com.bkboiz.shopappbackend.service.ICategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

@Log4j
@Service
@RequiredArgsConstructor
public class CategoryService implements ICategoryService {

    private final CategoryRepository categoryRepo;

    @Override
    public Category create(CategoryDTO categoryDTO) {
        Category cate = Category.builder().name(categoryDTO.getName()).build();
        return categoryRepo.save(cate);
    }

    @Override
    public Category getCategoryById(Long id) {
        return categoryRepo.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Can not find category with id = " + id));
    }

    @Override
    public Object findAll() {
        return categoryRepo.findAll().stream()
                .map(category -> CategoryResponse.builder()
                        .id(category.getId())
                        .name(category.getName())
                        .build())
                .toList();
    }
}
