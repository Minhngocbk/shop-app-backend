package com.bkboiz.shopappbackend.service.impl;

import com.bkboiz.shopappbackend.entity.Role;
import com.bkboiz.shopappbackend.repository.RoleRepository;
import com.bkboiz.shopappbackend.service.IRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j
@Service
@RequiredArgsConstructor
public class RoleService implements IRoleService {
    private final RoleRepository roleRepo;

    @Override
    public List<Role> findAll() {
        return roleRepo.findAll();
    }
}
