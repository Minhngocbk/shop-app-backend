package com.bkboiz.shopappbackend.service;

import com.bkboiz.shopappbackend.dto.OrderDTO;
import com.bkboiz.shopappbackend.dto.request.OrderRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IOrderService {
    Object createOrder(OrderDTO orderDTO);
    Object updateOrder(Long id, OrderDTO orderDTO);
    OrderDTO getOrder(Long orderId);
    List<OrderDTO> findOrderByUserId(Long userId);

    Object order(OrderRequest orderRequest);
}
