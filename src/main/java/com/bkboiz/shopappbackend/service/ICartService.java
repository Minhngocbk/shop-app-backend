package com.bkboiz.shopappbackend.service;

import com.bkboiz.shopappbackend.dto.CartDTO;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

@Service
public interface ICartService {
    Object findByUserId(Long userId);

    Object addToCart(@Valid CartDTO cartDTO);
}
