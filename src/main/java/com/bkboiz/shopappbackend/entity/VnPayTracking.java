package com.bkboiz.shopappbackend.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "vnpay_tracking")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VnPayTracking extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long orderId;
    private String refId;
    private String orderInfo;
    private Float amount;
    private String bankCode;
    private String command;
    private String currCode;
    private String locale;
    private String ipAddr;
    private String orderType;
    private String returnUrl;
    private String tmnCode;
    private String version;
    private String secureHash;
    private String createDate;
    private String expireDate;
}
