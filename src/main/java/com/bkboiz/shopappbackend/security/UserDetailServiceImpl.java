package com.bkboiz.shopappbackend.security;

import com.bkboiz.shopappbackend.entity.Role;
import com.bkboiz.shopappbackend.entity.User;
import com.bkboiz.shopappbackend.repository.RoleRepository;
import com.bkboiz.shopappbackend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.coyote.BadRequestException;
import org.hibernate.exception.DataException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {
    private final UserRepository userRepo;
    private final RoleRepository roleRepo;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByPhoneNumber(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid phone number"));

        Role role = roleRepo.findById(user.getRoleId())
                .orElseThrow(() -> new BadRequestException("Role not exist"));

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role.getName()));

        return new UserDetail(user.getPhoneNumber(), user.getPassword(), authorities);
    }
}
