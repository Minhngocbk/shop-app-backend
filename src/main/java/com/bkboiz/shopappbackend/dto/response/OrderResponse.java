package com.bkboiz.shopappbackend.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class OrderResponse {
    private Integer userId;
    private String fullName;
    private String phoneNumber;
    private String address;
    private String note;
    private String shippingMethod;
    private String shippingAddress;
    private Date shippingDate;
    private String trackingNumber;
    private String paymentMethod;
    private Float totalMoney;
    private Date orderDate;
    private String status;
}
