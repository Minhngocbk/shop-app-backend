package com.bkboiz.shopappbackend.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProductResponse {
    private Long id;
    private String name;
    private String description;
    private String thumbnail;
    private Float price;
    private String categoryName;
    private List<String> productImages;
}
