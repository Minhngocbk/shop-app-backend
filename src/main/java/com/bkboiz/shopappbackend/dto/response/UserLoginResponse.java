package com.bkboiz.shopappbackend.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserLoginResponse {
    private String accessToken;
    private String phoneNumber;
    private String fullName;
    private String roleName;
    private Long userId;
}
