package com.bkboiz.shopappbackend.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ListProductResponse {
    private int totalPages;
    private List<ProductResponse> lstProduct;
}
