package com.bkboiz.shopappbackend.dto.response;

import lombok.*;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartResponse {
    private Long productId;
    private Long quantity;
}
