package com.bkboiz.shopappbackend.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDetailRequest {
    @NotNull(message = "Product id can not be null")
    private Long productId;

    @NotNull(message = "Price can not be null")
    private Float price;

    @NotNull(message = "Quantity can not be null")
    private Long quantity;

    @NotNull(message = "Total money can not be null")
    private Float totalMoney;
}

