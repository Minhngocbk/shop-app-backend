package com.bkboiz.shopappbackend.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO {

    @NotNull(message = "User id can't be null")
    private Long userId;

    @NotNull(message = "Product id can't be null")
    private Long productId;

    @NotNull(message = "Quantity can't be null")
    private Long quantity;
}
