package com.bkboiz.shopappbackend.utils;

import com.bkboiz.shopappbackend.dto.request.OrderRequest;
import com.bkboiz.shopappbackend.entity.VnPayTracking;
import com.bkboiz.shopappbackend.service.IVnPayTrackingService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

@Log4j
@Component
@RequiredArgsConstructor
public class VNPay {

    @Value("${vnpay.url.pay}")
    private String urlPay;

    @Value("${vnpay.url.return}")
    private String urlReturn;

    @Value("${vnpay.attr.version}")
    private String vnPayVersion;

    @Value("${vnpay.attr.command}")
    private String vnPayCommand;

    @Value("${vnpay.attr.orderType}")
    private String vnPayOrderType;

    @Value("${vnpay.attr.tmnCode}")
    private String vnPayTmnCode;

    @Value("${vnpay.attr.secretKey}")
    private String vnPaySecretKey;

    private final Gson gson;
    private final IVnPayTrackingService vnPayTrackingService;

    @SneakyThrows
    public String putParam(OrderRequest orderRequest, Long orderId) {
        String refId = VNPayConfig.getRandomNumber(8);
        String vnpOrderInfo = "Thanh toan don hang " + refId + ". So tien: " + orderRequest.getTotalMoney();
        String vnpIpAddr = "127.0.0.1";

        long amount = (long) (orderRequest.getTotalMoney() * 100L);

        Map<String, String> params = new HashMap<>();

        params.put("vnp_Version", vnPayVersion);
        params.put("vnp_Command", vnPayCommand);
        params.put("vnp_TmnCode", vnPayTmnCode);
        params.put("vnp_Amount", String.valueOf(amount));
        params.put("vnp_CurrCode", "VND");

        String bankCode = orderRequest.getBankCode();
        if (bankCode != null && !bankCode.isEmpty()) {
            params.put("vnp_BankCode", bankCode);
        }

//      Mã tham chiếu của giao dịch tại hệ thống của merchant.
//      Mã này là duy nhất dùng để phân biệt các đơn hàng gửi sang VNPAY.
//      Không được trùng lặp trong ngày.
        params.put("vnp_TxnRef", refId);

        params.put("vnp_OrderInfo", vnpOrderInfo);
        params.put("vnp_OrderType", vnPayOrderType);

        String locate = LocaleContextHolder.getLocale().toString();
        if (!locate.isEmpty()) {
            params.put("vnp_Locale", locate);
        } else {
            params.put("vnp_Locale", "vn");
        }

//      URL thông báo kết quả giao dịch khi Khách hàng kết thúc thanh toán
        params.put("vnp_ReturnUrl", urlReturn);

        params.put("vnp_IpAddr", vnpIpAddr);

        Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String vnpCreateDate = formatter.format(cld.getTime());

        // Là thời gian phát sinh giao dịch định dạng yyyyMMddHHmmss (Time zone GMT+7)
        params.put("vnp_CreateDate", vnpCreateDate);
        cld.add(Calendar.MINUTE, 15);
        String vnpExpireDate = formatter.format(cld.getTime());

        // Thời gian hết hạn thanh toán GMT+7, định dạng: yyyyMMddHHmmss
        params.put("vnp_ExpireDate", vnpExpireDate);

        //Build data to hash and querystring
        List<String> fieldNames = new ArrayList<>(params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator<String> itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = itr.next();
            String fieldValue = params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                //Build query
                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
                query.append('=');
                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnpSecureHash = VNPayConfig.hmacSHA512(vnPaySecretKey, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnpSecureHash;
        String paymentUrl = urlPay + "?" + queryUrl;
        com.google.gson.JsonObject job = new JsonObject();
        job.addProperty("code", "00");
        job.addProperty("message", "Success");
        job.addProperty("data", paymentUrl);
        String s = gson.toJson(job);
        System.out.println(paymentUrl);

        VnPayTracking tracking = VnPayTracking.builder()
                .orderId(orderId)
                .refId(refId)
                .orderInfo(vnpOrderInfo)
                .amount(orderRequest.getTotalMoney())
                .bankCode(bankCode)
                .command(vnPayCommand)
                .currCode("VND")
                .ipAddr(vnpIpAddr)
                .locale(locate)
                .orderType(vnPayOrderType)
                .returnUrl(paymentUrl)
                .tmnCode(vnPayTmnCode)
                .version(vnPayVersion)
                .createDate(vnpCreateDate)
                .expireDate(vnpExpireDate)
                .secureHash(vnpSecureHash)
                .build();
        vnPayTrackingService.save(tracking);

        return s;
    }
}
